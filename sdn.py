import os
from glob import glob
import socket
import struct
import argparse
import json
import subprocess

import cmd2
from jinja2 import Template

class SSHExecutor(object):
    def __init__(self, host, user='root'):
        self.host = host
        self.user = user
        self.conf_file = open('%s.conf' % self.host, 'a')

    def execute(self, cmd):
        print('[%s]: %s' % (self.host, cmd))
        self.conf_file.write(cmd + '\n')

    def commit(self, host):
        proc = subprocess.Popen('scp %s.conf %s@%s:' % (host, self.user, host), shell=True)
        proc.wait()
        proc = subprocess.Popen('ssh %s@%s bash %s.conf' %(self.user, host, host), shell=True)
        proc.wait()
        self.conf_file.seek(0)
        self.conf_file.truncate(0)

Executor = SSHExecutor
#======================#
# Global Config actions
#======================#
class Config(object):
    def __init__(self):
        self.config = {
                "hosts": [],
                "routers": [],
                "networks": [],
                "vms": []}

CFG = Config().config


class SDNCore(object):

    def __init__(self):
        self.executors = dict()

    def _get_ip_at_index(self, net_prefix, index):
        net_start = net_prefix.split('/')[0]
        start_ip = socket.inet_aton(net_start)
        lo_ip = struct.unpack("!L", start_ip)[0] + index
        return socket.inet_ntoa(struct.pack('!L', lo_ip))

    def add_network(self, net_name, net_prefix, hosts, routers):
        host_br_ip = self._get_ip_at_index(net_prefix, 2)
        net_gateway = self._get_ip_at_index(net_prefix, 1)
        net_mask = net_prefix.split('/')[-1]
        sdn_table = 'sdn_net'
        sdn_ipset = 'sdn_net'
        host_template = """
        ip link add br_{{ net_name }} type bridge
        ip addr add dev br_{{ net_name }} {{ host_br_ip }}/{{ net_mask }}
        ip link set br_{{ net_name }} up
        echo 1 > /proc/sys/net/ipv4/conf/br_{{ net_name }}/proxy_arp
        echo 1 > /proc/sys/net/ipv4/ip_forward
        grep -q sdn_net /etc/iproute2/rt_tables || echo "101 {{ sdn_table }}" >> /etc/iproute2/rt_tables
        {% for router in routers -%}
        ip route add {{ net_prefix }} via {{ router }} table {{ sdn_table }}
        ip route show table {{ sdn_table }}|egrep -q "default"| ip route add default via {{ router }} table {{ sdn_table }}
        {% endfor %}
        ip rule add iif br_{{ net_name }} lookup {{ sdn_table }}
        """
        router_template = """
        echo 1 > /proc/sys/net/ipv4/ip_forward
        ip addr add dev lo {{ net_gateway }}/32
        ipset list|egrep -q "Name:\s+{{ sdn_ipset }}" || ipset create {{ sdn_ipset }} hash:net
        iptables-save -t nat|egrep -q "match-set\s+{{ sdn_ipset }}\s+src\s+-j\s+MASQUERADE" || iptables -t nat -A POSTROUTING -m set --match-set {{ sdn_ipset }} src -j MASQUERADE
        """
        if host_template:
            for host in hosts:
                self.apply_config(host, Template(host_template).render(locals()))
        if router_template:
            for router in routers:
                self.apply_config(router, Template(router_template).render(locals()))

    def connect_internet(self, net_prefix, hosts, routers):
        sdn_ipset = 'sdn_net'
        router_template = """
        ipset -A {{ sdn_ipset }} {{ net_prefix }}
        """
        if router_template:
            for router in routers:
                self.apply_config(router, Template(router_template).render(locals()))

    def add_vm(self, vm_name, vm_ip, vm_host, net_name, net_prefix, hosts, routers):
        net_mask = net_prefix.split('/')[-1]
        net_gateway = self._get_ip_at_index(net_prefix, 1)
        host_template = """
        ip netns add {{ vm_name }}
        ip link add {{ net_name }}_{{ vm_name }} type veth peer name {{ net_name }}_{{ vm_name }}-eth0
        ip link set {{net_name}}_{{ vm_name }}-eth0 netns {{ vm_name }}
        ip netns exec {{ vm_name }} ip addr add dev {{ net_name}}_{{ vm_name }}-eth0 {{ vm_ip }}/{{ net_mask }}
        ip netns exec {{ vm_name }} ip link set {{ net_name}}_{{ vm_name }}-eth0 up
        ip netns exec {{ vm_name }} ip link set lo up
        ip netns exec {{ vm_name }} ip route add default via {{ net_gateway }}
        ip link set {{ net_name }}_{{ vm_name }} master br_{{ net_name }}
        ip link set {{ net_name}}_{{ vm_name }} up
        """
        router_template = """
        ip route add {{ vm_ip }}/32 via {{ vm_host }}
        """
        if host_template:
            self.apply_config(vm_host, Template(host_template).render(locals()))
        if router_template:
            for router in routers:
                self.apply_config(router, Template(router_template).render(locals()))

    def del_network(self, net_name, net_prefix, hosts, routers):
        host_template = """
        {% for router in routers %}
            ip route del {{ net_prefix }} via {{ router }}
        {% endfor %}
        echo 0 > /proc/sys/net/ipv4/conf/br_{{ net_name }}/proxy_arp
        ip link set br_{{ net_name }} down
        ip link del br_{{ net_name }} type bridge
        """
        router_template = """
        """
        if host_template:
            for host in hosts:
                self.apply_config(host, Template(host_template).render(locals()))
        if router_template:
            for router in routers:
                self.apply_config(router, Template(router_template).render(locals()))

    def del_vm(self, vm_name, vm_ip, vm_host, net_name, net_prefix, hosts, routers):
        host_template = """
        ip route del {{ vm_ip }}/32 dev br_{{ net_name }}
        ip link set {{ net_name }}{{ vm_name }} down
        ip link del {{ net_name }}{{ vm_name }} type veth peer name {{ net_name }}{{ vm_name }}-eth0
        ip netns del {{ vm_name }}
        """

        router_template = """
        ip route del {{ vm_ip }}/32 via {{ vm_host }}
        """
        if host_template:
            self.apply_config(vm_host, Template(host_template).render(locals()))
        if router_template:
            for router in routers:
                self.apply_config(router, Template(router_template).render(locals()))

    def get_executor(self, host):
        if host not in self.executors or self.executors[host] is not None:
            self.executors[host] = Executor(host)
        return self.executors[host]

    def apply_config(self, host, config):
        executor = self.get_executor(host)
        for line in config.split('\n'):
            line = line.strip(' ')
            if line.startswith('#'): continue
            executor.execute(line)

    def commit(self):
        for host in CFG["hosts"] + CFG["routers"]:
            executor = self.get_executor(host).commit(host)


class SDNShell(cmd2.Cmd):

    prompt = 'SDNShell > '
    sdncore = SDNCore()
    
    #==============#
    # Admin actions
    #==============#
    def print_output(self, data):
        if not data:
            return
        print(json.dumps(data, indent=4))

    host_parser = argparse.ArgumentParser()
    host_parser.add_argument('hostname')

    @cmd2.with_argparser(host_parser)
    def do_add_host(self, args):
        CFG["hosts"].append(args.hostname)
        self.print_output({'Host': args.hostname})
    
    def do_list_hosts(self, args):
        self.print_output(CFG["hosts"])

    @cmd2.with_argparser(host_parser)
    def do_add_router(self, args):
        CFG["routers"].append(args.hostname)
        self.print_output({'Router': args.hostname})
    
    def do_list_routers(self, args):
        self.print_output(CFG["routers"])

    #==============#
    # User actions
    #==============#
    net_parser = argparse.ArgumentParser()
    net_parser.add_argument('net_prefix')
    net_parser.add_argument('net_name')

    @cmd2.with_argparser(net_parser)
    def do_add_network(self, args):
        if not self.validate():
            return
        net = self._get_item(CFG["networks"], 'name', args.net_name)
        if net:
            print("Network name exists")
            self.print_output(net)
            return
        net = self._get_item(CFG["networks"], 'prefix', args.net_prefix)
        if net:
            print("Network prefix exists")
            self.print_output(net)
            return
        CFG["networks"].append({"name": args.net_name, "prefix": args.net_prefix, "internet_access": False})
        self.sdncore.add_network(args.net_name, args.net_prefix, CFG['hosts'], CFG['routers'])
        self.print_output({"name": args.net_name, "prefix": args.net_prefix})
    
    net_name_parser = argparse.ArgumentParser()
    net_name_parser.add_argument('net_name')

    @cmd2.with_argparser(net_name_parser)
    def do_del_network(self, args):
        self.sdncore.del_network(args.net_name, args.net_prefix, CFG['hosts'], CFG['routers'])
        net = self._get_item(CFG["networks"], 'name', args.net_name)
        CFG["networks"].remove(net)
    
    @cmd2.with_argparser(net_name_parser)
    def do_connect_internet(self, args):
        net = self._get_item(CFG["networks"], 'name', args.net_name)
        if not net:
            print("Network %s does not exist" % args.net_name)
            return
        self.sdncore.connect_internet(net["prefix"], CFG['hosts'], CFG['routers'])
        net["internet_access"] = True

    def _get_item(self, item_list, key, value):
        for item in item_list:
            if item[key] == value:
                return item

    def do_list_networks(self, args):
        self.print_output(CFG["networks"])

    vm_parser = argparse.ArgumentParser()
    vm_parser.add_argument('vm_name')
    vm_parser.add_argument('vm_ip')
    vm_parser.add_argument('vm_host')
    vm_parser.add_argument('net_name')

    @cmd2.with_argparser(vm_parser)
    def do_add_vm(self, args):
        if not self.validate():
            return
        vm = self._get_item(CFG["vms"], 'name', args.vm_name)
        if vm:
            print("VM name exists")
            self.print_output(vm)
            return
        vm = self._get_item(CFG["vms"], 'ip', args.vm_ip)
        if vm:
            print("VM name exists")
            self.print_output(vm)
            return
        net = self._get_item(CFG["networks"], 'name', args.net_name)
        if not net:
            print("Network %s does not exist" % args.net_name)
            return
        net_prefix = net["prefix"]
        if not args.vm_host in CFG['hosts']:
            print("Host %s does not exist" % args.vm_host)
            return
        self.sdncore.add_vm(args.vm_name, args.vm_ip, args.vm_host, args.net_name, net_prefix, CFG['hosts'], CFG['routers'])
        CFG["vms"].append({"name": args.vm_name, "ip": args.vm_ip, "host": args.vm_host, "network": args.net_name})
        self.print_output({"name": args.vm_name, "ip": args.vm_ip, "host": args.vm_host, "network": args.net_name})
    
    @cmd2.with_argparser(vm_parser)
    def do_del_vm(self, args):
        net_prefix = [net["prefix"] for net in CFG["networks"] if net["name"] == args.net_name ][0]
        self.sdncore.del_vm(args.vm_name, args.vm_ip, args.vm_host, args.net_name, net_prefix, CFG['hosts'], CFG['routers'])
        CFG["vms"].remove({"name": args.vm_name, "ip": args.vm_ip, "host": args.vm_host, "network": args.net_name})

    def do_list_vms(self, args):
        self.print_output(CFG["vms"])

    def do_show_config(self, args):
        self.print_output(CFG)

    def do_show_hosts_config(self, args):
        self.print_output(self.sdncore.host_configs)

    def do_commit(self, args):
        self.sdncore.commit()

    def postloop(self):
        resp = raw_input('Would like to overwrite privious config [y/n]')
        if resp != 'y':
            return True
        with open('.sdnconfig', 'w') as sdncfg:
            print('Writing config ...')
            sdncfg.write(json.dumps(CFG, indent=4))
        return True

    def preloop(self):
        for _file in glob('*.conf'):
            os.unlink(_file)

        try:
            cfg = json.loads(open('.sdnconfig').read())
        except Exception as e:
            print('No previous config could be loaded')
            return True
        print('Previous config found')
        self.print_output(cfg)
        resp = raw_input('Would like to load [y/n]')
        if resp != 'y':
            return True
        if not self.validate(cfg):
            return
        print("---------------")
        print("Adding networks")
        print("---------------")
        for network in cfg['networks']:
            print('............')
            print('Net: %s' % network['name'])
            print('............')
            self.sdncore.add_network(network['name'], network['prefix'], cfg['hosts'], cfg['routers'])
        print("---------------")
        print("Adding vms")
        print("---------------")
        for vm in cfg['vms']:
            print('............')
            print('VM: %s' % vm['name'])
            print('............')
            net_prefix = [net["prefix"] for net in cfg["networks"] if net["name"] == vm['network']][0]
            self.sdncore.add_vm(vm['name'], vm['ip'], vm['host'], vm['network'], net_prefix, cfg['hosts'], cfg['routers'])
        global CFG
        CFG = cfg

    def validate(self, conf=None):
        if conf is None:
            conf=CFG
        if not conf.get('routers', []) or not conf.get('hosts', []):
            print('You need to add hosts and routers before adding networks and vms')
            return False
        return True

if __name__ == '__main__':
    SDNShell().cmdloop()
