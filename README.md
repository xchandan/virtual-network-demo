Here is an example of how to use the python script

You will need jinja2 and cmd2 python modules. 

Run the following
```
python sdn.py
```
Use the following command to add the Hosts, router and VMs
```
SDNShell > add_router 192.168.56.101
SDNShell > add_host 192.168.56.103
SDNShell > add_host 192.168.56.104

SDNShell > add_network 1.1.1.0/24 net1
SDNShell > add_vm vm1-1 1.1.1.10 192.168.56.103 net1
SDNShell > add_vm vm2-1 1.1.1.11 192.168.56.103 net1
SDNShell > add_vm vm3-1 1.1.1.12 192.168.56.104 net1
SDNShell > add_vm vm4-1 1.1.1.13 192.168.56.104 net1

SDNShell > add_network 1.1.2.0/24 net2
SDNShell > add_vm vm1-2 1.1.2.10 192.168.56.103 net2
SDNShell > add_vm vm2-2 1.1.2.11 192.168.56.103 net2
SDNShell > add_vm vm3-2 1.1.2.12 192.168.56.104 net2
SDNShell > add_vm vm4-2 1.1.2.13 192.168.56.104 net2
SDNShell > commit
```
To access the VMs use the ip netns command on the host
```
ip netns exec vm1-1 /bin/bash
```
From here you should be able to ping other VMs on the remote host.
